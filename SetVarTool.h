#ifndef SETVARTOOL_H
#define SETVARTOOL_H

#include "VariableElement.h"
#include <wx/msgdlg.h>
#include <string>
#include <vector>

//(*Headers(SetVarTool)
#include <wx/panel.h>
#include <wx/grid.h>
#include <wx/button.h>
#include <wx/frame.h>
//*)

class SetVarTool: public wxFrame
{
	public:

		SetVarTool(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize);
		virtual ~SetVarTool();

		//(*Declarations(SetVarTool)
		wxButton* btnAddRow;
		wxPanel* Panel1;
		wxGrid* Grid1;
		wxButton* btnSaveExit;
		//*)

	protected:

		//(*Identifiers(SetVarTool)
		static const long ID_GRID1;
		static const long ID_BUTTON1;
		static const long ID_BUTTON2;
		static const long ID_PANEL1;
		//*)

	private:
	    void AddBlankRow();
	    void DeleteRow(int rowPos);
        int rowCount;
        void LoadCurrentValues();
		//(*Handlers(SetVarTool)
		void OnbtnAddRowClick(wxCommandEvent& event);
		void OnbtnSaveExitClick(wxCommandEvent& event);
		//*)

		DECLARE_EVENT_TABLE()
};

#endif
