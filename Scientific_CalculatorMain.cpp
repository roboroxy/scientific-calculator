/***************************************************************
 * Name:      Scientific_CalculatorMain.cpp
 * Purpose:   Code for Application Frame
 * Author:    Dilshan Jayaweera (dilaaaajaya@hotmail.com)
 * Created:   2015-06-15
 * Copyright: Dilshan Jayaweera ()
 * License:
 **************************************************************/

#include "Scientific_CalculatorMain.h"

//(*InternalHeaders(Scientific_CalculatorFrame)
#include <wx/bitmap.h>
#include <wx/intl.h>
#include <wx/image.h>
#include <wx/string.h>
//*)

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//(*IdInit(Scientific_CalculatorFrame)
const long Scientific_CalculatorFrame::ID_TEXTCTRL1 = wxNewId();
const long Scientific_CalculatorFrame::ID_PANEL1 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON1 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON2 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON3 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON34 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON4 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON5 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON6 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON7 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON8 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON9 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON10 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON11 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON12 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON13 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON14 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON15 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON16 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON17 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON18 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON19 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON20 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON21 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON22 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON23 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON24 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON25 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON26 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON27 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON28 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON29 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON30 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON31 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON32 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON33 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON35 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON36 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON38 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON39 = wxNewId();
const long Scientific_CalculatorFrame::ID_BITMAPBUTTON37 = wxNewId();
const long Scientific_CalculatorFrame::ID_PANEL2 = wxNewId();
const long Scientific_CalculatorFrame::idMenuQuit = wxNewId();
const long Scientific_CalculatorFrame::ID_MENUITEM1 = wxNewId();
const long Scientific_CalculatorFrame::idMenuAbout = wxNewId();
const long Scientific_CalculatorFrame::ID_STATUSBAR1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(Scientific_CalculatorFrame,wxFrame)
    //(*EventTable(Scientific_CalculatorFrame)
    //*)
END_EVENT_TABLE()

Scientific_CalculatorFrame::Scientific_CalculatorFrame(wxWindow* parent,wxWindowID id)
{
    //(*Initialize(Scientific_CalculatorFrame)
    wxMenuItem* MenuItem2;
    wxGridBagSizer* GridBagSizer1;
    wxGridBagSizer* GridBagSizer2;
    wxMenuItem* MenuItem1;
    wxMenu* Menu1;
    wxBoxSizer* BoxSizer1;
    wxMenuBar* MenuBar1;
    wxMenu* Menu2;
    wxGridBagSizer* GridBagSizer3;

    Create(parent, id, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("id"));
    GridBagSizer1 = new wxGridBagSizer(0, 0);
    Panel1 = new wxPanel(this, ID_PANEL1, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("ID_PANEL1"));
    BoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
    txtDisplayPanel = new wxTextCtrl(Panel1, ID_TEXTCTRL1, wxEmptyString, wxDefaultPosition, wxSize(500,22), wxTE_RIGHT, wxDefaultValidator, _T("ID_TEXTCTRL1"));
    BoxSizer1->Add(txtDisplayPanel, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    Panel1->SetSizer(BoxSizer1);
    BoxSizer1->Fit(Panel1);
    BoxSizer1->SetSizeHints(Panel1);
    GridBagSizer1->Add(Panel1, wxGBPosition(0, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    GridBagSizer2 = new wxGridBagSizer(0, 0);
    Panel2 = new wxPanel(this, ID_PANEL2, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("ID_PANEL2"));
    GridBagSizer3 = new wxGridBagSizer(0, 0);
    btnFactorial = new wxBitmapButton(Panel2, ID_BITMAPBUTTON1, wxBitmap(wxImage(_T("images\\button_factorial.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON1"));
    btnFactorial->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_factorial_pressed.png"))));
    GridBagSizer3->Add(btnFactorial, wxGBPosition(0, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btn1 = new wxBitmapButton(Panel2, ID_BITMAPBUTTON2, wxBitmap(wxImage(_T("images\\button_1.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON2"));
    btn1->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_1_pressed.png"))));
    GridBagSizer3->Add(btn1, wxGBPosition(3, 3), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btn2 = new wxBitmapButton(Panel2, ID_BITMAPBUTTON3, wxBitmap(wxImage(_T("images\\button_2.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON3"));
    btn2->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_2_pressed.png"))));
    GridBagSizer3->Add(btn2, wxGBPosition(3, 4), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnCbrt = new wxBitmapButton(Panel2, ID_BITMAPBUTTON34, wxBitmap(wxImage(_T("images\\button_cbrt.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON34"));
    btnCbrt->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_cbrt_pressed.png"))));
    GridBagSizer3->Add(btnCbrt, wxGBPosition(5, 3), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btn3 = new wxBitmapButton(Panel2, ID_BITMAPBUTTON4, wxBitmap(wxImage(_T("images\\button_3.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON4"));
    btn3->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_3_pressed.png"))));
    GridBagSizer3->Add(btn3, wxGBPosition(3, 5), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btn0 = new wxBitmapButton(Panel2, ID_BITMAPBUTTON5, wxBitmap(wxImage(_T("images\\button_0.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON5"));
    btn0->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_0_pressed.png"))));
    GridBagSizer3->Add(btn0, wxGBPosition(4, 3), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnOpBracket = new wxBitmapButton(Panel2, ID_BITMAPBUTTON6, wxBitmap(wxImage(_T("images\\button_bracket_open.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON6"));
    btnOpBracket->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_bracket_open_pressed.png"))));
    GridBagSizer3->Add(btnOpBracket, wxGBPosition(0, 1), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnClBracket = new wxBitmapButton(Panel2, ID_BITMAPBUTTON7, wxBitmap(wxImage(_T("images\\button_bracket_close.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON7"));
    btnClBracket->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_bracket_close_pressed.png"))));
    GridBagSizer3->Add(btnClBracket, wxGBPosition(0, 2), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnPercent = new wxBitmapButton(Panel2, ID_BITMAPBUTTON8, wxBitmap(wxImage(_T("images\\button_percent.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON8"));
    btnPercent->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_percent_pressed.png"))));
    GridBagSizer3->Add(btnPercent, wxGBPosition(0, 3), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnAC = new wxBitmapButton(Panel2, ID_BITMAPBUTTON9, wxBitmap(wxImage(_T("images\\button_AC.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON9"));
    btnAC->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_AC_pressed.png"))));
    GridBagSizer3->Add(btnAC, wxGBPosition(0, 6), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnSin = new wxBitmapButton(Panel2, ID_BITMAPBUTTON10, wxBitmap(wxImage(_T("images\\button_sin.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON10"));
    btnSin->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_sin_pressed.png"))));
    GridBagSizer3->Add(btnSin, wxGBPosition(1, 1), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnCos = new wxBitmapButton(Panel2, ID_BITMAPBUTTON11, wxBitmap(wxImage(_T("images\\button_cos.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON11"));
    btnCos->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_cos_pressed.png"))));
    GridBagSizer3->Add(btnCos, wxGBPosition(2, 1), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnTan = new wxBitmapButton(Panel2, ID_BITMAPBUTTON12, wxBitmap(wxImage(_T("images\\button_tan.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON12"));
    btnTan->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_tan_pressed.png"))));
    GridBagSizer3->Add(btnTan, wxGBPosition(3, 1), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btn4 = new wxBitmapButton(Panel2, ID_BITMAPBUTTON13, wxBitmap(wxImage(_T("images\\button_4.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON13"));
    btn4->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_4_pressed.png"))));
    GridBagSizer3->Add(btn4, wxGBPosition(2, 3), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btn5 = new wxBitmapButton(Panel2, ID_BITMAPBUTTON14, wxBitmap(wxImage(_T("images\\button_5.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON14"));
    btn5->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_5_pressed.png"))));
    GridBagSizer3->Add(btn5, wxGBPosition(2, 4), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btn6 = new wxBitmapButton(Panel2, ID_BITMAPBUTTON15, wxBitmap(wxImage(_T("images\\button_6.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON15"));
    btn6->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_6_pressed.png"))));
    GridBagSizer3->Add(btn6, wxGBPosition(2, 5), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btn7 = new wxBitmapButton(Panel2, ID_BITMAPBUTTON16, wxBitmap(wxImage(_T("images\\button_7.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON16"));
    btn7->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_7_pressed.png"))));
    GridBagSizer3->Add(btn7, wxGBPosition(1, 3), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btn8 = new wxBitmapButton(Panel2, ID_BITMAPBUTTON17, wxBitmap(wxImage(_T("images\\button_8.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON17"));
    btn8->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_8_pressed.png"))));
    GridBagSizer3->Add(btn8, wxGBPosition(1, 4), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btn9 = new wxBitmapButton(Panel2, ID_BITMAPBUTTON18, wxBitmap(wxImage(_T("images\\button_9.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON18"));
    btn9->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_9_pressed.png"))));
    GridBagSizer3->Add(btn9, wxGBPosition(1, 5), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnDivide = new wxBitmapButton(Panel2, ID_BITMAPBUTTON19, wxBitmap(wxImage(_T("images\\button_divide.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON19"));
    btnDivide->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_divide_pressed.png"))));
    GridBagSizer3->Add(btnDivide, wxGBPosition(1, 6), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnMultiply = new wxBitmapButton(Panel2, ID_BITMAPBUTTON20, wxBitmap(wxImage(_T("images\\button_multiply.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON20"));
    btnMultiply->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_multiply_pressed.png"))));
    GridBagSizer3->Add(btnMultiply, wxGBPosition(2, 6), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnMinus = new wxBitmapButton(Panel2, ID_BITMAPBUTTON21, wxBitmap(wxImage(_T("images\\button_minus.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON21"));
    btnMinus->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_minus_pressed.png"))));
    GridBagSizer3->Add(btnMinus, wxGBPosition(3, 6), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnAdd = new wxBitmapButton(Panel2, ID_BITMAPBUTTON22, wxBitmap(wxImage(_T("images\\button_plus.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON22"));
    btnAdd->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_plus_pressed.png"))));
    GridBagSizer3->Add(btnAdd, wxGBPosition(4, 6), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnEqual = new wxBitmapButton(Panel2, ID_BITMAPBUTTON23, wxBitmap(wxImage(_T("images\\button_equal.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON23"));
    btnEqual->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_equal_pressed.png"))));
    GridBagSizer3->Add(btnEqual, wxGBPosition(4, 5), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnDot = new wxBitmapButton(Panel2, ID_BITMAPBUTTON24, wxBitmap(wxImage(_T("images\\button_dot.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON24"));
    btnDot->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_dot_pressed.png"))));
    GridBagSizer3->Add(btnDot, wxGBPosition(4, 4), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnPower = new wxBitmapButton(Panel2, ID_BITMAPBUTTON25, wxBitmap(wxImage(_T("images\\button_power.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON25"));
    btnPower->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_power_pressed.png"))));
    GridBagSizer3->Add(btnPower, wxGBPosition(4, 2), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnSqrt = new wxBitmapButton(Panel2, ID_BITMAPBUTTON26, wxBitmap(wxImage(_T("images\\button_sqrt.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON26"));
    btnSqrt->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_sqrt_pressed.png"))));
    GridBagSizer3->Add(btnSqrt, wxGBPosition(3, 2), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnLog = new wxBitmapButton(Panel2, ID_BITMAPBUTTON27, wxBitmap(wxImage(_T("images\\button_log.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON27"));
    btnLog->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_log_pressed.png"))));
    GridBagSizer3->Add(btnLog, wxGBPosition(2, 2), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnSinh = new wxBitmapButton(Panel2, ID_BITMAPBUTTON28, wxBitmap(wxImage(_T("images\\button_sinh.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON28"));
    btnSinh->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_sinh_pressed.png"))));
    GridBagSizer3->Add(btnSinh, wxGBPosition(1, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnCosh = new wxBitmapButton(Panel2, ID_BITMAPBUTTON29, wxBitmap(wxImage(_T("images\\button_cosh.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON29"));
    btnCosh->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_cosh_pressed.png"))));
    GridBagSizer3->Add(btnCosh, wxGBPosition(2, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnTanh = new wxBitmapButton(Panel2, ID_BITMAPBUTTON30, wxBitmap(wxImage(_T("images\\button_tanh.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON30"));
    btnTanh->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_tanh_pressed.png"))));
    GridBagSizer3->Add(btnTanh, wxGBPosition(3, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnMod = new wxBitmapButton(Panel2, ID_BITMAPBUTTON31, wxBitmap(wxImage(_T("images\\button_mod.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON31"));
    btnMod->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_mod_pressed.png"))));
    GridBagSizer3->Add(btnMod, wxGBPosition(1, 2), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnNpr = new wxBitmapButton(Panel2, ID_BITMAPBUTTON32, wxBitmap(wxImage(_T("images\\button_nPr.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON32"));
    btnNpr->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_nPr_pressed.png"))));
    GridBagSizer3->Add(btnNpr, wxGBPosition(4, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnNcr = new wxBitmapButton(Panel2, ID_BITMAPBUTTON33, wxBitmap(wxImage(_T("images\\button_nCr.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON33"));
    btnNcr->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_nCr_pressed.png"))));
    GridBagSizer3->Add(btnNcr, wxGBPosition(4, 1), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnCeil = new wxBitmapButton(Panel2, ID_BITMAPBUTTON35, wxBitmap(wxImage(_T("images\\button_ceil.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON35"));
    btnCeil->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_ceil_pressed.png"))));
    GridBagSizer3->Add(btnCeil, wxGBPosition(5, 4), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnFloor = new wxBitmapButton(Panel2, ID_BITMAPBUTTON36, wxBitmap(wxImage(_T("images\\button_floor.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON36"));
    btnFloor->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_floor_pressed.png"))));
    GridBagSizer3->Add(btnFloor, wxGBPosition(5, 5), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnPi = new wxBitmapButton(Panel2, ID_BITMAPBUTTON38, wxBitmap(wxImage(_T("images\\button_pi.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON38"));
    btnPi->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_pi_pressed.png"))));
    GridBagSizer3->Add(btnPi, wxGBPosition(5, 2), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnRound = new wxBitmapButton(Panel2, ID_BITMAPBUTTON39, wxBitmap(wxImage(_T("images\\button_round.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON39"));
    btnRound->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_round_pressed.png"))));
    GridBagSizer3->Add(btnRound, wxGBPosition(5, 6), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    btnAbs = new wxBitmapButton(Panel2, ID_BITMAPBUTTON37, wxBitmap(wxImage(_T("images\\button_abs.png"))), wxDefaultPosition, wxSize(50,30), wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON37"));
    btnAbs->SetBitmapSelected(wxBitmap(wxImage(_T("images\\button_abs_pressed.png"))));
    GridBagSizer3->Add(btnAbs, wxGBPosition(5, 1), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    Panel2->SetSizer(GridBagSizer3);
    GridBagSizer3->Fit(Panel2);
    GridBagSizer3->SetSizeHints(Panel2);
    GridBagSizer2->Add(Panel2, wxGBPosition(0, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    GridBagSizer1->Add(GridBagSizer2, wxGBPosition(1, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    SetSizer(GridBagSizer1);
    MenuBar1 = new wxMenuBar();
    Menu1 = new wxMenu();
    MenuItem1 = new wxMenuItem(Menu1, idMenuQuit, _("Quit\tAlt-F4"), _("Quit the application"), wxITEM_NORMAL);
    Menu1->Append(MenuItem1);
    MenuBar1->Append(Menu1, _("&File"));
    Menu3 = new wxMenu();
    MenuItem3 = new wxMenuItem(Menu3, ID_MENUITEM1, _("Set Variables"), wxEmptyString, wxITEM_NORMAL);
    Menu3->Append(MenuItem3);
    MenuBar1->Append(Menu3, _("Tools"));
    Menu2 = new wxMenu();
    MenuItem2 = new wxMenuItem(Menu2, idMenuAbout, _("About\tF1"), _("Show info about this application"), wxITEM_NORMAL);
    Menu2->Append(MenuItem2);
    MenuBar1->Append(Menu2, _("Help"));
    SetMenuBar(MenuBar1);
    StatusBar1 = new wxStatusBar(this, ID_STATUSBAR1, 0, _T("ID_STATUSBAR1"));
    int __wxStatusBarWidths_1[1] = { -1 };
    int __wxStatusBarStyles_1[1] = { wxSB_NORMAL };
    StatusBar1->SetFieldsCount(1,__wxStatusBarWidths_1);
    StatusBar1->SetStatusStyles(1,__wxStatusBarStyles_1);
    SetStatusBar(StatusBar1);
    GridBagSizer1->Fit(this);
    GridBagSizer1->SetSizeHints(this);

    Connect(ID_BITMAPBUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnFactorialClick);
    Connect(ID_BITMAPBUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::Onbtn1Click);
    Connect(ID_BITMAPBUTTON3,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::Onbtn2Click);
    Connect(ID_BITMAPBUTTON34,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnCbrtClick);
    Connect(ID_BITMAPBUTTON4,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::Onbtn3Click);
    Connect(ID_BITMAPBUTTON5,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::Onbtn0Click);
    Connect(ID_BITMAPBUTTON6,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnOpBracketClick);
    Connect(ID_BITMAPBUTTON7,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnClBracketClick);
    Connect(ID_BITMAPBUTTON8,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnPercentClick);
    Connect(ID_BITMAPBUTTON9,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnACClick);
    Connect(ID_BITMAPBUTTON10,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnSinClick);
    Connect(ID_BITMAPBUTTON11,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnCosClick);
    Connect(ID_BITMAPBUTTON12,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnTanClick);
    Connect(ID_BITMAPBUTTON13,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::Onbtn4Click);
    Connect(ID_BITMAPBUTTON14,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::Onbtn5Click);
    Connect(ID_BITMAPBUTTON15,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::Onbtn6Click);
    Connect(ID_BITMAPBUTTON16,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::Onbtn7Click);
    Connect(ID_BITMAPBUTTON17,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::Onbtn8Click);
    Connect(ID_BITMAPBUTTON18,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::Onbtn9Click);
    Connect(ID_BITMAPBUTTON19,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnDivideClick);
    Connect(ID_BITMAPBUTTON20,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnMultiplyClick);
    Connect(ID_BITMAPBUTTON21,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnMinusClick);
    Connect(ID_BITMAPBUTTON22,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnAddClick);
    Connect(ID_BITMAPBUTTON23,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnEqualClick);
    Connect(ID_BITMAPBUTTON24,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnDotClick);
    Connect(ID_BITMAPBUTTON25,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnPowerClick);
    Connect(ID_BITMAPBUTTON26,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnSqrtClick);
    Connect(ID_BITMAPBUTTON27,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnLogClick);
    Connect(ID_BITMAPBUTTON28,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnSinhClick);
    Connect(ID_BITMAPBUTTON29,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnCoshClick);
    Connect(ID_BITMAPBUTTON30,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnTanhClick);
    Connect(ID_BITMAPBUTTON31,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnModClick);
    Connect(ID_BITMAPBUTTON32,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnNprClick);
    Connect(ID_BITMAPBUTTON33,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnNcrClick);
    Connect(ID_BITMAPBUTTON35,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnCeilClick);
    Connect(ID_BITMAPBUTTON36,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnFloorClick);
    Connect(ID_BITMAPBUTTON38,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnPiClick);
    Connect(ID_BITMAPBUTTON39,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnRoundClick);
    Connect(ID_BITMAPBUTTON37,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnbtnAbsClick);
    Connect(idMenuQuit,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnQuit);
    Connect(idMenuAbout,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&Scientific_CalculatorFrame::OnAbout);
    //*)

    Bind(wxEVT_COMMAND_MENU_SELECTED, &Scientific_CalculatorFrame::OnMenuItemClick, this, ID_MENUITEM1);
}

Scientific_CalculatorFrame::~Scientific_CalculatorFrame()
{
    //(*Destroy(Scientific_CalculatorFrame)
    //*)
}


void Scientific_CalculatorFrame::OnMenuItemClick(wxCommandEvent& event) {
    if(event.GetId() == ID_MENUITEM1) {
        SetVarTool *varFrame = new SetVarTool(0);
        varFrame->Show();
    }
}

void Scientific_CalculatorFrame::OnQuit(wxCommandEvent& event)
{
    Close();
}

void Scientific_CalculatorFrame::OnAbout(wxCommandEvent& event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}

void Scientific_CalculatorFrame::OnbtnACClick(wxCommandEvent& event)
{
    txtDisplayPanel->SetValue("");
}

void Scientific_CalculatorFrame::Onbtn0Click(wxCommandEvent& event)
{
    wxString strFormula = txtDisplayPanel->GetValue() + "0";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::Onbtn1Click(wxCommandEvent& event)
{
    wxString strFormula = txtDisplayPanel->GetValue() + "1";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::Onbtn2Click(wxCommandEvent& event)
{
    wxString strFormula = txtDisplayPanel->GetValue() + "2";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::Onbtn3Click(wxCommandEvent& event)
{
    wxString strFormula = txtDisplayPanel->GetValue() + "3";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::Onbtn4Click(wxCommandEvent& event)
{
    wxString strFormula = txtDisplayPanel->GetValue() + "4";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::Onbtn5Click(wxCommandEvent& event)
{
    wxString strFormula = txtDisplayPanel->GetValue() + "5";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::Onbtn6Click(wxCommandEvent& event)
{
    wxString strFormula = txtDisplayPanel->GetValue() + "6";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::Onbtn7Click(wxCommandEvent& event)
{
    wxString strFormula = txtDisplayPanel->GetValue() + "7";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::Onbtn8Click(wxCommandEvent& event)
{
    wxString strFormula = txtDisplayPanel->GetValue() + "8";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::Onbtn9Click(wxCommandEvent& event)
{
    wxString strFormula = txtDisplayPanel->GetValue() + "9";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnDotClick(wxCommandEvent& event)
{
    std::string stdstrFormula = txtDisplayPanel->GetValue().ToStdString();
    size_t dotCount = std::count(stdstrFormula.begin(), stdstrFormula.end(), '.');

    if(dotCount <= 0) {
        wxString strFormula = stdstrFormula + ".";
        txtDisplayPanel->SetValue(strFormula);
    }
}

void Scientific_CalculatorFrame::OnbtnAddClick(wxCommandEvent& event)
{
    PlusFunctionElement plusEl;
    wxString strFormula = txtDisplayPanel->GetValue() + plusEl.getSign();
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnMinusClick(wxCommandEvent& event)
{
    MinusFunctionElement minEl;
    wxString strFormula = txtDisplayPanel->GetValue() + minEl.getSign();
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnMultiplyClick(wxCommandEvent& event)
{
    MultipleFunctionElement multplEl;
    wxString strFormula = txtDisplayPanel->GetValue() + multplEl.getSign();
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnDivideClick(wxCommandEvent& event)
{
    DivideFunctionElement divdEl;
    wxString strFormula = txtDisplayPanel->GetValue() + divdEl.getSign();
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnFactorialClick(wxCommandEvent& event)
{
    FactorialElement factoEl;
    wxString strFormula = txtDisplayPanel->GetValue() + factoEl.getSign();
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnOpBracketClick(wxCommandEvent& event)
{
    wxString strFormula = txtDisplayPanel->GetValue() + "(";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnClBracketClick(wxCommandEvent& event)
{
    wxString strFormula = txtDisplayPanel->GetValue() + ")";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnPercentClick(wxCommandEvent& event)
{
    wxString strFormula = txtDisplayPanel->GetValue() + "%";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnSinhClick(wxCommandEvent& event)
{
    SinehFunctionElement sinhEl;
    wxString strFormula = txtDisplayPanel->GetValue() + sinhEl.getSign() + "()";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnSinClick(wxCommandEvent& event)
{
    SineFunctionElement sinEl;
    wxString strFormula = txtDisplayPanel->GetValue() + sinEl.getSign() + "()";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnCoshClick(wxCommandEvent& event)
{
    CoshFunctionElement coshEl;
    wxString strFormula = txtDisplayPanel->GetValue() + coshEl.getSign() + "()";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnCosClick(wxCommandEvent& event)
{
    CosineFunctionElement cosEl;
    wxString strFormula = txtDisplayPanel->GetValue() + cosEl.getSign() + "()";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnTanhClick(wxCommandEvent& event)
{
    TanhFunctionElement tanhEl;
    wxString strFormula = txtDisplayPanel->GetValue() + tanhEl.getSign() + "()";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnTanClick(wxCommandEvent& event)
{
    TanFunctionElement tanEl;
    wxString strFormula = txtDisplayPanel->GetValue() + tanEl.getSign() + "()";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnNprClick(wxCommandEvent& event)
{
    NprFunctionElement nPrEl;
    wxString strFormula = txtDisplayPanel->GetValue() + nPrEl.getSign() + "(,)";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnNcrClick(wxCommandEvent& event)
{
    NcrFunctionElement nCrEl;
    wxString strFormula = txtDisplayPanel->GetValue() + nCrEl.getSign() + "(,)";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnModClick(wxCommandEvent& event)
{
    ModFunctionElement modEl;
    wxString strFormula = txtDisplayPanel->GetValue() + modEl.getSign();
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnLogClick(wxCommandEvent& event)
{
    LogFunctionElement logEl;
    wxString strFormula = txtDisplayPanel->GetValue() + logEl.getSign() + "()";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnSqrtClick(wxCommandEvent& event)
{
    SquareRootFunctionElement sqrtEl;
    wxString strFormula = txtDisplayPanel->GetValue() + sqrtEl.getSign() + "()";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnPowerClick(wxCommandEvent& event)
{
    PowFunctionElement powrEl;
    wxString strFormula = txtDisplayPanel->GetValue() + powrEl.getSign();
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnAbsClick(wxCommandEvent& event)
{
    AbsfunctionElement absEl;
    wxString strFormula = txtDisplayPanel->GetValue() + absEl.getSign() + "()";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnPiClick(wxCommandEvent& event)
{
    PIFunctionElement piEl;
    wxString strFormula = txtDisplayPanel->GetValue() + piEl.getSign() + "()";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnCbrtClick(wxCommandEvent& event)
{
    CubeRootFunctionElement cbrtEl;
    wxString strFormula = txtDisplayPanel->GetValue() + cbrtEl.getSign() + "()";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnCeilClick(wxCommandEvent& event)
{
    CeliFunctionElement ceilEl;
    wxString strFormula = txtDisplayPanel->GetValue() + ceilEl.getSign() + "()";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnFloorClick(wxCommandEvent& event)
{
    FloorFunctionElement floorEl;
    wxString strFormula = txtDisplayPanel->GetValue() + floorEl.getSign() + "()";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnRoundClick(wxCommandEvent& event)
{
    RoundFunctionElement roundEl;
    wxString strFormula = txtDisplayPanel->GetValue() + roundEl.getSign() + "()";
    txtDisplayPanel->SetValue(strFormula);
}

void Scientific_CalculatorFrame::OnbtnEqualClick(wxCommandEvent& event)
{
    std::string stdstrFormula = txtDisplayPanel->GetValue().ToStdString();
    ExpressionBuilder *exprBuilder = new ExpressionBuilder(stdstrFormula);
    FormulaElement* expr = exprBuilder->build();
    double Ansvalue = expr->evaluate();;
    wxString Ansstring;
    Ansstring << Ansvalue;
    txtDisplayPanel->SetValue(Ansstring);
}
