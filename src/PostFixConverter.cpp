#include "PostFixConverter.h"

PostFixConverter::PostFixConverter()
{
    //ctor
    this->delimiter = " "; // default delimiter
}


void PostFixConverter::setDelimiter(string delimiter){

    this->delimiter = delimiter;
}


string PostFixConverter::getDelimiter(){


    return this->delimiter;

}

bool PostFixConverter::isOperator(string token){


  return token == "+"  ||
            token == "-"  ||
            token == "/"  ||
            token == "*"  ||
            token == "^" ||
            token == "cos"||
            token == "floor"||
            token == "round"||
            token == "npr"||
            token == "abs"||
            token == "ceil"||
            token == "cosh"||
            token == "cbrt"||
            token == "log"||
            token == "mod"||
            token == "ncr"||
            token == "pi"||
            token == "sin"||
            token == "sinh"||
            token == "sqrt"||
            token == "tan"||
            token == "tanh"||
            token == "!";



}


int PostFixConverter::getWeight(string op){


    int weight = 0;

    if(op == "+" || op == "-"){

        weight = 1;
    }

    else if(op == "*" || op == "/"){

        weight = 2;

    }
    else if(op == "^"){

         weight = 3;

    }
    else{

         weight = 4;

    }

   return weight;


}



bool PostFixConverter::isHigher(string op1, string op2){


     // if op1 has higher weight than op2 return true

   	int op1Weight = getWeight(op1);
	int op2Weight = getWeight(op2);

	if(op1Weight == op2Weight) return true;
	return op1Weight > op2Weight ?  true: false;



}


string PostFixConverter::toPostfix(string infix){



    MathTokenizer *tokenizer = new MathTokenizer(infix);
    vector<string> tokens = tokenizer->getTokens(); // break things into tokens 5 + 5 + log ( 3 )



      stack<string> tran_stack;

     string postfix = "";
     string delimiter = this->getDelimiter(); // delimiter can be used to split the postfix notation for evaluation purpose


    for(std::vector<string>::const_iterator token = tokens.begin();token != tokens.end(); ++token)
    {
        // get current token
        string t = *token;


        if(isOperator(t)){


            // operator detected


            while(!tran_stack.empty() && tran_stack.top() != "(" && isHigher(tran_stack.top(),t))
			{

                // Operator on the stack  have higher precedence, so

				postfix.append(tran_stack.top());
				postfix.append(delimiter);

				tran_stack.pop();
			}

			tran_stack.push(t); // put lower operand



        }

        else if (t == "("){


            tran_stack.push(t);


        }
        else if(t == ")")
		{
		    // pop out all the operators inside ()

            while(!tran_stack.empty() && tran_stack.top() !=  "(") {

                // append all the elements until ( found

				postfix.append(tran_stack.top());
				postfix.append(delimiter);
				tran_stack.pop();
			}
			tran_stack.pop(); // remove ( from stack

		}
		else{

            // if value append it to postfix string

            postfix.append(t);
            postfix.append(delimiter);

        }







    }


     while(!tran_stack.empty()) {

            postfix.append(tran_stack.top());
            postfix.append(delimiter);
            tran_stack.pop();
     }

   return postfix;






}
