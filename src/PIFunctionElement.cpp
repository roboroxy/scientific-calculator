#include "PIFunctionElement.h"

PIFunctionElement::PIFunctionElement()
{
    //ctor
}


double PIFunctionElement::evaluate(){


    return atan(this->getArguments().at(0)->evaluate()) * 4;
}

string PIFunctionElement::getSign(){


    return "pi";


}



string PIFunctionElement::toString(){


     return this->getSign()+"("+this->getArguments().at(0)->toString()+")";


}
