#include "TanFunctionElement.h"

TanFunctionElement::TanFunctionElement()
{
    //ctor
}


double TanFunctionElement::evaluate(){

   return tan(this->getArguments().at(0)->evaluate() * 3.14159 / 180.0);


}

string TanFunctionElement::getSign(){


    return "tan";


}



string TanFunctionElement::toString(){


     return this->getSign()+"("+this->getArguments().at(0)->toString()+")";


}
