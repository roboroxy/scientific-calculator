#include "TanhFunctionElement.h"

TanhFunctionElement::TanhFunctionElement()
{
    //ctor
}

double TanhFunctionElement::evaluate(){

   return tanh(this->getArguments().at(0)->evaluate() * 3.14159 / 180.0);


}

string TanhFunctionElement::getSign(){


    return "tanh";


}



string TanhFunctionElement::toString(){


     return this->getSign()+"("+this->getArguments().at(0)->toString()+")";


}
