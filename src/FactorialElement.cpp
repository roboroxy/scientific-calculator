#include "FactorialElement.h"

FactorialElement::FactorialElement()
{
    //ctor
}





double FactorialElement::evaluate(){

     return this->factorial(this->getArguments().at(0)->evaluate());

}


string FactorialElement::getSign(){


    return "!";


}



double FactorialElement::factorial(double n)
{
  return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}



string FactorialElement::toString(){


    return this->getArguments().at(0)->toString()+this->getSign();

}
