#include "SineFunctionElement.h"
#include <math.h>

SineFunctionElement::SineFunctionElement()
{
    //ctor
}

double SineFunctionElement::evaluate(){

      return sin(this->getArguments().at(0)->evaluate() * 3.14159 / 180.0);

}

string SineFunctionElement::getSign(){


    return "sin";


}


string SineFunctionElement::toString(){


    return this->getSign()+"("+this->getArguments().at(0)->toString()+")";

}
