#include "RoundFunctionElement.h"

RoundFunctionElement::RoundFunctionElement()
{
    //ctor
}

double RoundFunctionElement::evaluate(){

   return round(this->getArguments().at(0)->evaluate());


}

string RoundFunctionElement::getSign(){


    return "round";


}



string RoundFunctionElement::toString(){


     return this->getSign()+"("+this->getArguments().at(0)->toString()+")";


}
