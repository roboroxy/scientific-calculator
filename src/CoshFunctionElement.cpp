#include "CoshFunctionElement.h"

CoshFunctionElement::CoshFunctionElement()
{
    //ctor
}




double CoshFunctionElement::evaluate(){

   return cosh(this->getArguments().at(0)->evaluate() * 3.14159 / 180.0);


}

string CoshFunctionElement::getSign(){


    return "cosh";


}



string CoshFunctionElement::toString(){


     return this->getSign()+"("+this->getArguments().at(0)->toString()+")";


}
