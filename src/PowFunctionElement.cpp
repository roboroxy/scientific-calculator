#include "PowFunctionElement.h"

PowFunctionElement::PowFunctionElement()
{
    //ctor
}

double PowFunctionElement::evaluate(){

    return pow(this->getArguments().at(0)->evaluate(),this->getArguments().at(1)->evaluate());

}


string PowFunctionElement::getSign(){


    return "^";


}
