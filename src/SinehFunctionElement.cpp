#include "SinehFunctionElement.h"

SinehFunctionElement::SinehFunctionElement()
{
    //ctor
}


double SinehFunctionElement::evaluate(){

   return sinh(this->getArguments().at(0)->evaluate() * 3.14159 / 180.0);


}

string SinehFunctionElement::getSign(){


    return "sinh";


}



string SinehFunctionElement::toString(){


     return this->getSign()+"("+this->getArguments().at(0)->toString()+")";


}
