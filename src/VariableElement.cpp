/*
 * VariableElement.cpp
 *
 *  Created on: Jun 2, 2015
 *      Author: ASUS
 */

#include "VariableElement.h"

vector<VariableElement*> VariableElement::allVariables;

VariableElement::VariableElement() {


	this->variable = "";
	this->value = 0;

}


VariableElement::VariableElement(std::string variable){


	this->variable = variable;

}



VariableElement::VariableElement(std::string variable,double value){

	this->variable = variable;
	this->value = value;

}


std::string VariableElement::getVariable(){

	return this->variable;
}



double VariableElement::getValue(){

	return this->value;
}



void VariableElement::setValue(double value){

	 this->value = value;
}



double VariableElement::evaluate(){

    return this->getValue();

}

string VariableElement::toString(){

    return this->variable;

}

