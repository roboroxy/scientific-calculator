#include "CubeRootFunctionElement.h"

CubeRootFunctionElement::CubeRootFunctionElement()
{
    //ctor
}


double CubeRootFunctionElement::evaluate(){

   return cbrt(this->getArguments().at(0)->evaluate());


}

string CubeRootFunctionElement::getSign(){


    return "cbrt";


}



string CubeRootFunctionElement::toString(){


     return this->getSign()+"("+this->getArguments().at(0)->toString()+")";



}
