#include "AbsfunctionElement.h"

AbsfunctionElement::AbsfunctionElement()
{
    //ctor
}

double AbsfunctionElement::evaluate(){

   return abs(this->getArguments().at(0)->evaluate());


}

string AbsfunctionElement::getSign(){


    return "abs";


}



string AbsfunctionElement::toString(){


     return this->getSign()+"("+this->getArguments().at(0)->toString()+")";


}

