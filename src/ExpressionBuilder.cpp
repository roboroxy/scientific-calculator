#include "ExpressionBuilder.h"




ExpressionBuilder::ExpressionBuilder(string expression)
{

    this->expression = expression;
    //ctor
}


string ExpressionBuilder::getExpression(){


    return this->expression;

}



string ExpressionBuilder::getPostfixNotation(){

    return this->postFixNotation;
}


FormulaElement* ExpressionBuilder::build(){


    PostFixConverter *conveter = new PostFixConverter();
    this->postFixNotation  = conveter->toPostfix(this->expression); // get postfix string separated by a delimiter

    stringstream ss(this->postFixNotation);
    string current_token;

    // break postfix notation  using  delimiter
    vector<string> tokens;


    while(getline(ss, current_token,*conveter->getDelimiter().c_str())) {

		tokens.push_back(current_token);
	}




    //build tree from postfix notation
    for(std::vector<string>::const_iterator token = tokens.begin();token != tokens.end(); ++token)
    {


        if(PostFixConverter::isOperator(*token)){


             FormulaElement* topval = this->operatorStack.top(); // get top
             this->operatorStack.pop(); // remove top


           // operator detected
           if(*token == "+"){

                 FormulaElement* underplus = this->operatorStack.top(); // get top
                 this->operatorStack.pop(); // remove top

                 PlusFunctionElement *plus = new PlusFunctionElement();
                 plus->addArgument(underplus);
                 plus->addArgument(topval);
                 this->operatorStack.push(plus);


           }
           else if(*token == "-"){


                 FormulaElement* undermin = this->operatorStack.top(); // get top
                 this->operatorStack.pop(); // remove top

                 MinusFunctionElement *min = new MinusFunctionElement();
                 min->addArgument(undermin);
                 min->addArgument(topval);
                 this->operatorStack.push(min);

           }
           else if(*token == "/"){

                 FormulaElement* underdiv = this->operatorStack.top(); // get top
                 this->operatorStack.pop(); // remove top

                 DivideFunctionElement *div = new DivideFunctionElement();
                 div->addArgument(underdiv);
                 div->addArgument(topval);
                 this->operatorStack.push(div);

           }
           else if(*token == "*"){

                 FormulaElement* undermul = this->operatorStack.top(); // get top
                 this->operatorStack.pop(); // remove top

                 MultipleFunctionElement *mul = new MultipleFunctionElement();
                 mul->addArgument(undermul);
                 mul->addArgument(topval);
                 this->operatorStack.push(mul);


           }
           else if(*token == "^"){

                 FormulaElement* underpow = this->operatorStack.top(); // get top
                 this->operatorStack.pop(); // remove top

                 PowFunctionElement *power = new PowFunctionElement();
                 power->addArgument(underpow);
                 power->addArgument(topval);
                 this->operatorStack.push(power);


           }
            else if(*token == "npr"){

                 FormulaElement* underpow = this->operatorStack.top(); // get top
                 this->operatorStack.pop(); // remove top

                 NprFunctionElement *npr = new NprFunctionElement();
                 npr->addArgument(underpow);
                 npr->addArgument(topval);
                 this->operatorStack.push(npr);



           }

           else if(*token == "ncr"){

                 FormulaElement* underpow = this->operatorStack.top(); // get top
                 this->operatorStack.pop(); // remove top

                 NcrFunctionElement *ncr = new NcrFunctionElement();
                 ncr->addArgument(underpow);
                 ncr->addArgument(topval);
                 this->operatorStack.push(ncr);



           }

           else if(*token == "mod"){



                   FormulaElement* underpow = this->operatorStack.top(); // get top
                 this->operatorStack.pop(); // remove top

                 ModFunctionElement *mod = new ModFunctionElement();
                 mod->addArgument(underpow);
                 mod->addArgument(topval);
                 this->operatorStack.push(mod);



           }

           /* single arguments functions goes here */
           else if(*token == "cos"){


                CosineFunctionElement *cos = new CosineFunctionElement();
                cos->addArgument(topval);
                this->operatorStack.push(cos);



           }
            else if(*token == "floor"){



                FloorFunctionElement *floor = new FloorFunctionElement();
                floor->addArgument(topval);
                this->operatorStack.push(floor);



           }
           else if(*token == "round"){



                RoundFunctionElement *round = new RoundFunctionElement();
                round->addArgument(topval);
                this->operatorStack.push(round);



           }
           else if(*token == "!"){

                // factorial

                FactorialElement *factorial = new FactorialElement();
                factorial->addArgument(topval);
                this->operatorStack.push(factorial);



           }
           else if(*token == "abs"){



                AbsfunctionElement *abs = new AbsfunctionElement();
                abs->addArgument(topval);
                this->operatorStack.push(abs);



           }


            else if(*token == "ceil"){



                CeliFunctionElement *celi = new CeliFunctionElement();
                celi->addArgument(topval);
                this->operatorStack.push(celi);



           }



           else if(*token == "cosh"){



                CoshFunctionElement *cosh = new CoshFunctionElement();
                cosh->addArgument(topval);
                this->operatorStack.push(cosh);



           }


             else if(*token == "cbrt"){



                CubeRootFunctionElement *cube = new CubeRootFunctionElement();
                cube->addArgument(topval);
                this->operatorStack.push(cube);



           }


             else if(*token == "log"){



                LogFunctionElement *log = new LogFunctionElement();
                log->addArgument(topval);
                this->operatorStack.push(log);



           }



             else if(*token == "pi"){



                PIFunctionElement *PI = new PIFunctionElement();
                PI->addArgument(topval);
                this->operatorStack.push(PI);



           }

           else if(*token == "sin"){



                SineFunctionElement *sine = new SineFunctionElement();
                sine->addArgument(topval);
                this->operatorStack.push(sine);



           }

             else if(*token == "sinh"){



                SinehFunctionElement *sineh = new SinehFunctionElement();
                sineh->addArgument(topval);
                this->operatorStack.push(sineh);



           }


         else if(*token == "sqrt"){



                SquareRootFunctionElement *sqrt = new SquareRootFunctionElement();
                sqrt->addArgument(topval);
                this->operatorStack.push(sqrt);



           }


        else if(*token == "tan"){



                TanFunctionElement *tan = new TanFunctionElement();
                tan->addArgument(topval);
                this->operatorStack.push(tan);



           }

        else if(*token == "tanh"){



                TanhFunctionElement *tanh = new TanhFunctionElement();
                tanh->addArgument(topval);
                this->operatorStack.push(tanh);



           }




        }else{



            // Could be constant  value or variable


           string temp_token = *token;
            bool isavar = false;
            for(size_t pos = 0; (pos = temp_token.find_first_of("ABCDEFGHIJKLMNOPQRSTUVWXYZ", pos)) != std::string::npos; ++pos){

                // this is a variable

                isavar = true;
                string var(1,temp_token[pos]);

                double varVal = 0;
                if(VariableElement::allVariables.size() != 0)
                    for(VariableElement *varEl : VariableElement::allVariables)
                        if(varEl->getVariable() == var)
                            varVal = varEl->getValue();

                this->operatorStack.push(new VariableElement(var,varVal)); // at the moment let me assign 5 as a value of every variable element i will :D


            }

            if(isavar == false){

                // this is a number

                 double num = 0;

                 num = atof(temp_token.c_str());


                this->operatorStack.push(new ConstantElement(num));

            }






        }




    }





      FormulaElement* expressionTree = this->operatorStack.top(); // get top


      return expressionTree;




}
