

#include "ConstantElement.h"



ConstantElement::ConstantElement() {

	this->value = 0.0;

}

ConstantElement::ConstantElement(double value) {

	this->value = value;

}

double ConstantElement::getValue(){

	return this->value;
}


double ConstantElement::evaluate(){

    return this->getValue();

}



string ConstantElement::toString(){

    ostringstream ss;
    ss << this->value;

    return ss.str();

}
