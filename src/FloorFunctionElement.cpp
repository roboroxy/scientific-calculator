#include "FloorFunctionElement.h"

FloorFunctionElement::FloorFunctionElement()
{
    //ctor
}


double FloorFunctionElement::evaluate(){

   return floor (this->getArguments().at(0)->evaluate());



}

string FloorFunctionElement::getSign(){


    return "floor";


}



string FloorFunctionElement::toString(){


     return this->getSign()+"("+this->getArguments().at(0)->toString()+")";


}
