#include "NprFunctionElement.h"

NprFunctionElement::NprFunctionElement()
{
    //ctor
}



double NprFunctionElement::evaluate(){


    return FactorialElement::factorial(this->getArguments().at(0)->evaluate())/FactorialElement::factorial((this->getArguments().at(0)->evaluate() - this->getArguments().at(1)->evaluate()));
}


string NprFunctionElement::getSign(){


    return "npr";


}



string NprFunctionElement::toString(){


     return this->getSign()+"("+this->getArguments().at(0)->toString()+","+this->getArguments().at(1)->toString()+")";


}


