#include "LogFunctionElement.h"

LogFunctionElement::LogFunctionElement()
{
    //ctor
}


double LogFunctionElement::evaluate(){

   return log10 (this->getArguments().at(0)->evaluate());


}

string LogFunctionElement::getSign(){


    return "log";


}



string LogFunctionElement::toString(){


     return this->getSign()+"("+this->getArguments().at(0)->toString()+")";


}
