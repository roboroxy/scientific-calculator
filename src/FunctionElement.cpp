#include "FunctionElement.h"
#include <string>
#include <MultipleFunctionElement.h>
#include <ConstantElement.h>
#include <VariableElement.h>

FunctionElement::FunctionElement() {


}


void FunctionElement::addArgument(FormulaElement *argument){


	this->arguments.push_back(argument);
}



vector<FormulaElement*> FunctionElement::getArguments(){


	return this->arguments;
}



string FunctionElement::toString(){


     //ugly expression mode
     return "("+this->getArguments().at(0)->toString()+this->getSign()+this->getArguments().at(1)->toString()+")";


    string expression = "";

    FunctionElement* functionleft = dynamic_cast<FunctionElement*>(this->getArguments().at(0));
    FunctionElement* functionright = dynamic_cast<FunctionElement*>(this->getArguments().at(1));

    if(functionleft || functionright){


        // pretty parentheses


            string expr = "";

            expr+="(";
            expr +=  this->getArguments().at(0)->toString();
            expr += ")";


            expr += this->getSign();



            expr+="(";
            expr +=  this->getArguments().at(1)->toString();
            expr+= ")";

             expression =  expr;


    }else{

             // non functional objects goes here
             // else return as it is along with the sign

             string expr = "";

             expr+=this->getArguments().at(0)->toString();


             /* 2*X = 2X FIX */


             if(dynamic_cast<ConstantElement*>(this->getArguments().at(0)) && dynamic_cast<VariableElement*>(this->getArguments().at(1))){

                    // prevent from appending sign because we have left(constant) right(variable)
             }else{


                 expr+=this->getSign();


             }


             /* 2*X = 2X FIX */




             expr+=this->getArguments().at(1)->toString();


             expression = expr;

    }



    return expression;



}
