#include "CeliFunctionElement.h"

CeliFunctionElement::CeliFunctionElement()
{
    //ctor
}



double CeliFunctionElement::evaluate(){

   return ceil(this->getArguments().at(0)->evaluate());


}

string CeliFunctionElement::getSign(){


    return "ceil";


}



string CeliFunctionElement::toString(){


     return this->getSign()+"("+this->getArguments().at(0)->toString()+")";


}
