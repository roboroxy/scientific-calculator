#include "CosineFunctionElement.h"
#include <math.h>



CosineFunctionElement::CosineFunctionElement()
{
    //ctor
}

double CosineFunctionElement::evaluate(){

   return cos(this->getArguments().at(0)->evaluate() * 3.14159 / 180.0);


}

string CosineFunctionElement::getSign(){


    return "cos";


}



string CosineFunctionElement::toString(){


     return this->getSign()+"("+this->getArguments().at(0)->toString()+")";


}
