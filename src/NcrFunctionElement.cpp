#include "NcrFunctionElement.h"

NcrFunctionElement::NcrFunctionElement()
{
    //ctor
}




double NcrFunctionElement::evaluate(){

    //5! / (4! (5 - 4)!)

    double left = this->getArguments().at(0)->evaluate();
    double right = this->getArguments().at(1)->evaluate();

   double  nfac = FactorialElement::factorial(left);
   double cfac = FactorialElement::factorial(right);


   return nfac / (cfac * FactorialElement::factorial((left - right)));

}


string NcrFunctionElement::getSign(){


    return "ncr";


}



string NcrFunctionElement::toString(){


     return this->getSign()+"("+this->getArguments().at(0)->toString()+","+this->getArguments().at(1)->toString()+")";


}


