#include "SquareRootFunctionElement.h"

SquareRootFunctionElement::SquareRootFunctionElement()
{
    //ctor
}

double SquareRootFunctionElement::evaluate(){

   return sqrt(this->getArguments().at(0)->evaluate());


}

string SquareRootFunctionElement::getSign(){


    return "sqrt";


}



string SquareRootFunctionElement::toString(){


     return this->getSign()+"("+this->getArguments().at(0)->toString()+")";



}
