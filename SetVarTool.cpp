#include "SetVarTool.h"

//(*InternalHeaders(SetVarTool)
#include <wx/intl.h>
#include <wx/string.h>
//*)

//(*IdInit(SetVarTool)
const long SetVarTool::ID_GRID1 = wxNewId();
const long SetVarTool::ID_BUTTON1 = wxNewId();
const long SetVarTool::ID_BUTTON2 = wxNewId();
const long SetVarTool::ID_PANEL1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(SetVarTool,wxFrame)
	//(*EventTable(SetVarTool)
	//*)
END_EVENT_TABLE()

SetVarTool::SetVarTool(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
	//(*Initialize(SetVarTool)
	Create(parent, id, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("id"));
	SetClientSize(wxSize(270,300));
	Move(wxDefaultPosition);
	Panel1 = new wxPanel(this, ID_PANEL1, wxPoint(136,128), wxDefaultSize, wxTAB_TRAVERSAL, _T("ID_PANEL1"));
	Grid1 = new wxGrid(Panel1, ID_GRID1, wxPoint(8,8), wxSize(256,248), 0, _T("ID_GRID1"));
	Grid1->CreateGrid(1,2);
	Grid1->EnableEditing(true);
	Grid1->EnableGridLines(true);
	Grid1->SetColLabelValue(0, _("Var Name"));
	Grid1->SetColLabelValue(1, _("Var Value"));
	Grid1->SetDefaultCellFont( Grid1->GetFont() );
	Grid1->SetDefaultCellTextColour( Grid1->GetForegroundColour() );
	btnAddRow = new wxButton(Panel1, ID_BUTTON1, _("Add Row"), wxPoint(16,264), wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
	btnSaveExit = new wxButton(Panel1, ID_BUTTON2, _("Save and Exit"), wxPoint(120,264), wxSize(128,32), 0, wxDefaultValidator, _T("ID_BUTTON2"));

	Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&SetVarTool::OnbtnAddRowClick);
	Connect(ID_BUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&SetVarTool::OnbtnSaveExitClick);
	//*)

	this->rowCount = 1;
	LoadCurrentValues();
}

SetVarTool::~SetVarTool()
{
	//(*Destroy(SetVarTool)
	//*)
}

void SetVarTool::OnbtnAddRowClick(wxCommandEvent& event)
{
    AddBlankRow();
}

void SetVarTool::AddBlankRow() {
    Grid1->InsertRows();
    this->rowCount++;
}

void SetVarTool::DeleteRow(int rowPos) {
    Grid1->DeleteRows(rowPos);
    this->rowCount--;
}

void SetVarTool::LoadCurrentValues() {
    if(VariableElement::allVariables.size() != 0) {
        for(VariableElement *varEl : VariableElement::allVariables) {
            wxString VarName, VarValue;
            VarName << varEl->getVariable();
            VarValue << varEl->getValue();
            Grid1->SetCellValue(0, 0, VarName);
            Grid1->SetCellValue(0, 1, VarValue);
            AddBlankRow();
        }
        DeleteRow(0);
    }
}

void SetVarTool::OnbtnSaveExitClick(wxCommandEvent& event)
{
    VariableElement::allVariables.clear();
    for(int i = 0; i < this->rowCount; i++) {
        string strVarName = Grid1->GetCellValue(i, 0).ToStdString(), strVarValue = Grid1->GetCellValue(i, 1).ToStdString();
        double VarValue;
        if(strVarName != "" && strVarValue != "" && strVarName.size() == 1 && Grid1->GetCellValue(i, 1).ToDouble(&VarValue)) {
            VariableElement::allVariables.push_back(new VariableElement(strVarName, VarValue));
        }
    }
    Close();
}
