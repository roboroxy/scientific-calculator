#ifndef PIFUNCTIONELEMENT_H
#define PIFUNCTIONELEMENT_H

#include "FunctionElement.h"
#include <math.h>


class PIFunctionElement:public FunctionElement
{
    public:
        PIFunctionElement();
         double evaluate();
        string getSign();
        string toString();

};

#endif // PIFUNCTIONELEMENT_H
