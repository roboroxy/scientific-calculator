#ifndef FACTORIALELEMENT_H
#define FACTORIALELEMENT_H

#include "FunctionElement.h"

class FactorialElement:public FunctionElement
{

    public:
        FactorialElement();
        double evaluate();
	    string getSign();
        string toString();
        static double factorial(double n);

};

#endif // FACTORIALELEMENT_H
