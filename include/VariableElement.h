
#ifndef VARIABLEELEMENT_H_
#define VARIABLEELEMENT_H_

#include "FormulaElement.h"
#include <string>
#include <vector>

class VariableElement:public FormulaElement {

private:

	std::string variable;
	double value;

public:

	VariableElement(); // default
	VariableElement(std::string variable);
	VariableElement(std::string variable,double value);
	std::string getVariable();
	double getValue();
	double evaluate();
	void setValue(double value);
    string toString();
    static vector<VariableElement*> allVariables;
};

#endif /* VARIABLEELEMENT_H_ */
