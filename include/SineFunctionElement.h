#ifndef SINEFUNCTIONELEMENT_H
#define SINEFUNCTIONELEMENT_H

#include "FunctionElement.h"


class SineFunctionElement:public FunctionElement
{
    public:
        SineFunctionElement();
        double evaluate();
        string getSign();
        string toString();


};

#endif // SINEFUNCTIONELEMENT_H
