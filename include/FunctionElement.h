#ifndef FUNCTIONELEMENT_H
#define FUNCTIONELEMENT_H

#include "FormulaElement.h"
#include <string>
#include <vector>
#include <math.h>

using namespace std;

class FunctionElement :public FormulaElement
{
        protected:


        vector<FormulaElement*> arguments;

        public:
        FunctionElement();
        vector<FormulaElement*> getArguments();
        void addArgument(FormulaElement *argument);
        virtual string getSign(){};
        string toString();



};

#endif // FUNCTIONELEMENT_H
