#ifndef TANFUNCTIONELEMENT_H
#define TANFUNCTIONELEMENT_H

#include "FunctionElement.h"
#include <Math.h>

class TanFunctionElement:public FunctionElement
{
    public:
        TanFunctionElement();
        double evaluate();
        string getSign();
        string toString();

};

#endif // TANFUNCTIONELEMENT_H
