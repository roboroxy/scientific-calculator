

#ifndef CONSTANTELEMENT_H_
#define CONSTANTELEMENT_H_

#include "FormulaElement.h"
#include <string>
#include <sstream>

class ConstantElement:public FormulaElement
{

	private:

	double value;

	public:
		ConstantElement(); // default
		ConstantElement(double value);
		double evaluate();
		double getValue();
		string toString();


};

#endif /* CONSTANTELEMENT_H_ */
