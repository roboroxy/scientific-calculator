#ifndef FLOORFUNCTIONELEMENT_H
#define FLOORFUNCTIONELEMENT_H

#include "FunctionElement.h"
#include <math.h>

class FloorFunctionElement:public FunctionElement
{
    public:
        FloorFunctionElement();
         double evaluate();
        string getSign();
        string toString();

};

#endif // FLOORFUNCTIONELEMENT_H
