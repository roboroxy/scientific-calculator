

#ifndef DIVIDEFUNCTIONELEMENT_H_
#define DIVIDEFUNCTIONELEMENT_H_

#include <string>
#include "FunctionElement.h"

using namespace std;

class DivideFunctionElement:public FunctionElement {
public:
	DivideFunctionElement();
	double evaluate();
    string getSign();
};

#endif /* DIVIDEFUNCTIONELEMENT_H_ */
