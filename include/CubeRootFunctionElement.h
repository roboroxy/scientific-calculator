#ifndef CUBEROOTFUNCTIONELEMENT_H
#define CUBEROOTFUNCTIONELEMENT_H

#include "FunctionElement.h"
#include <math.h>

class CubeRootFunctionElement:public FunctionElement
{
    public:
        CubeRootFunctionElement();
         double evaluate();
        string getSign();
        string toString();

};

#endif // CUBEROOTFUNCTIONELEMENT_H
