

#ifndef PLUSFUNCTIONELEMENT_H_
#define PLUSFUNCTIONELEMENT_H_

#include "FunctionElement.h"

class PlusFunctionElement:public FunctionElement {
public:
	PlusFunctionElement();
	double evaluate();
	string getSign();
};

#endif /* PLUSFUNCTIONELEMENT_H_ */
