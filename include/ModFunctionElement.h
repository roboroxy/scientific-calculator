#ifndef MODFUNCTIONELEMENT_H
#define MODFUNCTIONELEMENT_H

#include "FunctionElement.h"

class ModFunctionElement:public FunctionElement
{
    public:
        ModFunctionElement();
        double evaluate();
        string getSign();

};

#endif // MODFUNCTIONELEMENT_H
