#ifndef NPRFUNCTIONELEMENT_H
#define NPRFUNCTIONELEMENT_H

#include "FunctionElement.h"
#include "FactorialElement.h"

class NprFunctionElement:public FunctionElement
{

    public:
        NprFunctionElement();
        double evaluate();
        string getSign();
         string toString();

};

#endif // NPRFUNCTIONELEMENT_H
