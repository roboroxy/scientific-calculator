
#ifndef MULTIPLEFUNCTIONELEMENT_H_
#define MULTIPLEFUNCTIONELEMENT_H_

#include "FunctionElement.h"

class MultipleFunctionElement:public FunctionElement {
public:
	MultipleFunctionElement();
	double evaluate();
    string getSign();
};

#endif /* MULTIPLEFUNCTIONELEMENT_H_ */
