#ifndef MATHTOKENIZER_H
#define MATHTOKENIZER_H

#include <vector>
#include <string>
#include <algorithm>
#include <string.h>

using namespace std;

class MathTokenizer
{
   private:
    string expression;
    public:
        MathTokenizer(string infixexpressions);
        vector<string> getTokens();

};

#endif // MATHTOKENIZER_H

