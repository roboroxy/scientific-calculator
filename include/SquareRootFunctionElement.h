#ifndef SQUAREROOTFUNCTIONELEMENT_H
#define SQUAREROOTFUNCTIONELEMENT_H


#include "FunctionElement.h"
#include <math.h>

class SquareRootFunctionElement:public FunctionElement
{
    public:
        SquareRootFunctionElement();
         double evaluate();
        string getSign();
        string toString();


};

#endif // SQUAREROOTFUNCTIONELEMENT_H
