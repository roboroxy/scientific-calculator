#ifndef ROUNDFUNCTIONELEMENT_H
#define ROUNDFUNCTIONELEMENT_H

#include "FunctionElement.h"
#include <Math.h>

class RoundFunctionElement:public FunctionElement
{
    public:
        RoundFunctionElement();
          double evaluate();
        string getSign();
        string toString();

};

#endif // ROUNDFUNCTIONELEMENT_H
