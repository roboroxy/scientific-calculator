#ifndef COSHFUNCTIONELEMENT_H
#define COSHFUNCTIONELEMENT_H

#include "FunctionElement.h"
#include <Math.h>

class CoshFunctionElement:public FunctionElement
{
    public:
        CoshFunctionElement();
        double evaluate();
        string getSign();
        string toString();

};

#endif // COSHFUNCTIONELEMENT_H
