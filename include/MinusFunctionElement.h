
#ifndef MINUSFUNCTIONELEMENT_H_
#define MINUSFUNCTIONELEMENT_H_

#include <string>
#include "FunctionElement.h"

class MinusFunctionElement:public FunctionElement {
public:
	MinusFunctionElement();
	double evaluate();
	string getSign();
};

#endif /* MINUSFUNCTIONELEMENT_H_ */
