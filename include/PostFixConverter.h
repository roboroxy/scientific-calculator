#ifndef POSTFIXCONVERTER_H
#define POSTFIXCONVERTER_H


#include <string>
#include <vector>
#include <stack>
#include "MathTokenizer.h"

using namespace std;

class PostFixConverter
{
    private:
     string delimiter;
    public:
        PostFixConverter();
        void setDelimiter(string delimiter);
        string getDelimiter();
        string toPostfix(string infix);
        static bool isOperator(string token);
        static int getWeight(string op);
        static bool isHigher(string op1, string op2);

};

#endif // POSTFIXCONVERTER_H
