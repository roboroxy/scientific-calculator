#ifndef TANHFUNCTIONELEMENT_H
#define TANHFUNCTIONELEMENT_H

#include "FunctionElement.h"
#include <Math.h>

class TanhFunctionElement:public FunctionElement
{
    public:
        TanhFunctionElement();
        double evaluate();
        string getSign();
        string toString();

};

#endif // TANHFUNCTIONELEMENT_H
