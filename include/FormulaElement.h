#ifndef FORMULAELEMENT_H
#define FORMULAELEMENT_H

#include <string>

using namespace std;

class FormulaElement
{
   private:

	public:
		FormulaElement();
		virtual ~FormulaElement(){};
		virtual double evaluate(){};
		virtual string toString(){};
};

#endif // FORMULAELEMENT_H
