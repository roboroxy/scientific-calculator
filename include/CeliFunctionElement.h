#ifndef CELIFUNCTIONELEMENT_H
#define CELIFUNCTIONELEMENT_H

#include "FunctionElement.h"
#include <Math.h>

class CeliFunctionElement:public FunctionElement
{
    public:
        CeliFunctionElement();
          double evaluate();
        string getSign();
        string toString();

};

#endif // CELIFUNCTIONELEMENT_H
