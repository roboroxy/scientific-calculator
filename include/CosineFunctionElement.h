#ifndef COSINEFUNCTIONELEMENT_H
#define COSINEFUNCTIONELEMENT_H

#include "FunctionElement.h"



class CosineFunctionElement:public FunctionElement
{

private:

    public:
        CosineFunctionElement();
        double evaluate();
        string getSign();
        string toString();


};

#endif // COSINEFUNCTIONELEMENT_H
