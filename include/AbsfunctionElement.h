#ifndef ABSFUNCTIONELEMENT_H
#define ABSFUNCTIONELEMENT_H


#include "FunctionElement.h"
#include <stdlib.h>


class AbsfunctionElement:public FunctionElement
{
    public:
        AbsfunctionElement();
        double evaluate();
        string getSign();
        string toString();


};

#endif // ABSFUNCTIONELEMENT_H
