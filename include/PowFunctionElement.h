#ifndef POWFUNCTIONELEMENT_H
#define POWFUNCTIONELEMENT_H

#include "FunctionElement.h"
#include <math.h>

class PowFunctionElement:public FunctionElement
{

    public:

        PowFunctionElement();
        double evaluate();
	    string getSign();


};

#endif // POWFUNCTIONELEMENT_H
