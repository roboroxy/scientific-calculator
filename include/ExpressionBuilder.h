#ifndef EXPRESSIONBUILDER_H
#define EXPRESSIONBUILDER_H

#include <iostream>
#include <FunctionElement.h>
#include <VariableElement.h>
#include <ConstantElement.h>
#include <FormulaElement.h>
#include <PostFixConverter.h>
#include <PlusFunctionElement.h>
#include <MinusFunctionElement.h>
#include <DivideFunctionElement.h>
#include <PowFunctionElement.h>
#include <MultipleFunctionElement.h>
#include <CosineFunctionElement.h>
#include <FloorFunctionElement.h>
#include <FactorialElement.h>
#include <RoundFunctionElement.h>
#include <NprFunctionElement.h>
#include <NcrFunctionElement.h>
#include <ModFunctionElement.h>
#include <AbsfunctionElement.h>
#include <CeliFunctionElement.h>
#include <CoshFunctionElement.h>
#include <CubeRootFunctionElement.h>
#include <LogFunctionElement.h>
#include <PIFunctionElement.h>
#include <SineFunctionElement.h>
#include <SinehFunctionElement.h>
#include <SquareRootFunctionElement.h>
#include <TanFunctionElement.h>
#include <TanhFunctionElement.h>
#include <stack>
#include <string>
#include <string.h>
#include <vector>
#include <stdlib.h>
#include <sstream>
#include <MathTokenizer.h>
#include<cstdlib>


using namespace std;

class ExpressionBuilder
{
    private:

    stack <FormulaElement*> operatorStack;
    string expression;
    string postFixNotation;

    public:
        ExpressionBuilder(string expr);
        string getExpression();
        string getPostfixNotation();
        FormulaElement* build();

};

#endif // EXPRESSIONBUILDER_H
