#ifndef NCRFUNCTIONELEMENT_H
#define NCRFUNCTIONELEMENT_H

#include "FunctionElement.h"
#include "FactorialElement.h"

class NcrFunctionElement:public FunctionElement
{
    public:
        NcrFunctionElement();
         double evaluate();
        string getSign();
         string toString();


};

#endif // NCRFUNCTIONELEMENT_H
