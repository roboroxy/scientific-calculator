#ifndef LOGFUNCTIONELEMENT_H
#define LOGFUNCTIONELEMENT_H

#include "FunctionElement.h"
#include <Math.h>

class LogFunctionElement:public FunctionElement
{
    public:
        LogFunctionElement();
        double evaluate();
        string getSign();
        string toString();

};

#endif // LOGFUNCTIONELEMENT_H
