#ifndef SINEHFUNCTIONELEMENT_H
#define SINEHFUNCTIONELEMENT_H

#include "FunctionElement.h"
#include <Math.h>

class SinehFunctionElement:public FunctionElement
{
    public:
        SinehFunctionElement();
        double evaluate();
        string getSign();
        string toString();

};

#endif // SINEHFUNCTIONELEMENT_H
