/***************************************************************
 * Name:      Scientific_CalculatorMain.h
 * Purpose:   Defines Application Frame
 * Author:    Dilshan Jayaweera (dilaaaajaya@hotmail.com)
 * Created:   2015-06-15
 * Copyright: Dilshan Jayaweera ()
 * License:
 **************************************************************/

#ifndef SCIENTIFIC_CALCULATORMAIN_H
#define SCIENTIFIC_CALCULATORMAIN_H

#include <wx/msgdlg.h>
#include <algorithm>
#include "PlusFunctionElement.h"
#include "MinusFunctionElement.h"
#include "MultipleFunctionElement.h"
#include "DivideFunctionElement.h"
#include "FactorialElement.h"
#include "SinehFunctionElement.h"
#include "SineFunctionElement.h"
#include "CoshFunctionElement.h"
#include "CosineFunctionElement.h"
#include "TanhFunctionElement.h"
#include "TanFunctionElement.h"
#include "NprFunctionElement.h"
#include "NcrFunctionElement.h"
#include "ModFunctionElement.h"
#include "LogFunctionElement.h"
#include "SquareRootFunctionElement.h"
#include "PowFunctionElement.h"
#include "AbsfunctionElement.h"
#include "PIFunctionElement.h"
#include "CubeRootFunctionElement.h"
#include "CeliFunctionElement.h"
#include "FloorFunctionElement.h"
#include "RoundFunctionElement.h"
#include "ExpressionBuilder.h"
#include "SetVarTool.h"

//(*Headers(Scientific_CalculatorFrame)
#include <wx/sizer.h>
#include <wx/menu.h>
#include <wx/textctrl.h>
#include <wx/panel.h>
#include <wx/bmpbuttn.h>
#include <wx/gbsizer.h>
#include <wx/frame.h>
#include <wx/statusbr.h>
//*)

class Scientific_CalculatorFrame: public wxFrame
{
    public:

        Scientific_CalculatorFrame(wxWindow* parent,wxWindowID id = -1);
        virtual ~Scientific_CalculatorFrame();

    private:

        //(*Handlers(Scientific_CalculatorFrame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnbtnACClick(wxCommandEvent& event);
        void Onbtn0Click(wxCommandEvent& event);
        void Onbtn1Click(wxCommandEvent& event);
        void Onbtn2Click(wxCommandEvent& event);
        void Onbtn3Click(wxCommandEvent& event);
        void Onbtn4Click(wxCommandEvent& event);
        void Onbtn5Click(wxCommandEvent& event);
        void Onbtn6Click(wxCommandEvent& event);
        void Onbtn7Click(wxCommandEvent& event);
        void Onbtn8Click(wxCommandEvent& event);
        void Onbtn9Click(wxCommandEvent& event);
        void OnbtnDotClick(wxCommandEvent& event);
        void OnbtnAddClick(wxCommandEvent& event);
        void OnbtnMinusClick(wxCommandEvent& event);
        void OnbtnMultiplyClick(wxCommandEvent& event);
        void OnbtnDivideClick(wxCommandEvent& event);
        void OnbtnFactorialClick(wxCommandEvent& event);
        void OnbtnOpBracketClick(wxCommandEvent& event);
        void OnbtnClBracketClick(wxCommandEvent& event);
        void OnbtnPercentClick(wxCommandEvent& event);
        void OnbtnSinhClick(wxCommandEvent& event);
        void OnbtnSinClick(wxCommandEvent& event);
        void OnbtnCoshClick(wxCommandEvent& event);
        void OnbtnCosClick(wxCommandEvent& event);
        void OnbtnTanhClick(wxCommandEvent& event);
        void OnbtnTanClick(wxCommandEvent& event);
        void OnbtnNprClick(wxCommandEvent& event);
        void OnbtnNcrClick(wxCommandEvent& event);
        void OnbtnModClick(wxCommandEvent& event);
        void OnbtnLogClick(wxCommandEvent& event);
        void OnbtnSqrtClick(wxCommandEvent& event);
        void OnbtnPowerClick(wxCommandEvent& event);
        void OnbtnAbsClick(wxCommandEvent& event);
        void OnbtnPiClick(wxCommandEvent& event);
        void OnbtnCbrtClick(wxCommandEvent& event);
        void OnbtnCeilClick(wxCommandEvent& event);
        void OnbtnFloorClick(wxCommandEvent& event);
        void OnbtnRoundClick(wxCommandEvent& event);
        void OnbtnEqualClick(wxCommandEvent& event);
        //*)

        //(*Identifiers(Scientific_CalculatorFrame)
        static const long ID_TEXTCTRL1;
        static const long ID_PANEL1;
        static const long ID_BITMAPBUTTON1;
        static const long ID_BITMAPBUTTON2;
        static const long ID_BITMAPBUTTON3;
        static const long ID_BITMAPBUTTON34;
        static const long ID_BITMAPBUTTON4;
        static const long ID_BITMAPBUTTON5;
        static const long ID_BITMAPBUTTON6;
        static const long ID_BITMAPBUTTON7;
        static const long ID_BITMAPBUTTON8;
        static const long ID_BITMAPBUTTON9;
        static const long ID_BITMAPBUTTON10;
        static const long ID_BITMAPBUTTON11;
        static const long ID_BITMAPBUTTON12;
        static const long ID_BITMAPBUTTON13;
        static const long ID_BITMAPBUTTON14;
        static const long ID_BITMAPBUTTON15;
        static const long ID_BITMAPBUTTON16;
        static const long ID_BITMAPBUTTON17;
        static const long ID_BITMAPBUTTON18;
        static const long ID_BITMAPBUTTON19;
        static const long ID_BITMAPBUTTON20;
        static const long ID_BITMAPBUTTON21;
        static const long ID_BITMAPBUTTON22;
        static const long ID_BITMAPBUTTON23;
        static const long ID_BITMAPBUTTON24;
        static const long ID_BITMAPBUTTON25;
        static const long ID_BITMAPBUTTON26;
        static const long ID_BITMAPBUTTON27;
        static const long ID_BITMAPBUTTON28;
        static const long ID_BITMAPBUTTON29;
        static const long ID_BITMAPBUTTON30;
        static const long ID_BITMAPBUTTON31;
        static const long ID_BITMAPBUTTON32;
        static const long ID_BITMAPBUTTON33;
        static const long ID_BITMAPBUTTON35;
        static const long ID_BITMAPBUTTON36;
        static const long ID_BITMAPBUTTON38;
        static const long ID_BITMAPBUTTON39;
        static const long ID_BITMAPBUTTON37;
        static const long ID_PANEL2;
        static const long idMenuQuit;
        static const long ID_MENUITEM1;
        static const long idMenuAbout;
        static const long ID_STATUSBAR1;
        //*)

        //(*Declarations(Scientific_CalculatorFrame)
        wxBitmapButton* btnNcr;
        wxBitmapButton* btnPi;
        wxBitmapButton* btnSin;
        wxBitmapButton* btnCeil;
        wxBitmapButton* btn6;
        wxTextCtrl* txtDisplayPanel;
        wxBitmapButton* btnMultiply;
        wxBitmapButton* btnDivide;
        wxBitmapButton* btnRound;
        wxMenu* Menu3;
        wxBitmapButton* btn0;
        wxBitmapButton* btnCosh;
        wxBitmapButton* btnLog;
        wxBitmapButton* btnCos;
        wxBitmapButton* btnAdd;
        wxBitmapButton* btn5;
        wxBitmapButton* btnPower;
        wxPanel* Panel1;
        wxBitmapButton* btnClBracket;
        wxBitmapButton* btn8;
        wxBitmapButton* btn7;
        wxBitmapButton* btnPercent;
        wxMenuItem* MenuItem3;
        wxBitmapButton* btnOpBracket;
        wxBitmapButton* btn2;
        wxStatusBar* StatusBar1;
        wxBitmapButton* btnFactorial;
        wxBitmapButton* btnSinh;
        wxBitmapButton* btnDot;
        wxBitmapButton* btnFloor;
        wxBitmapButton* btn4;
        wxBitmapButton* btnTan;
        wxBitmapButton* btnEqual;
        wxBitmapButton* btnTanh;
        wxBitmapButton* btnNpr;
        wxPanel* Panel2;
        wxBitmapButton* btn3;
        wxBitmapButton* btnMod;
        wxBitmapButton* btn9;
        wxBitmapButton* btnAbs;
        wxBitmapButton* btnMinus;
        wxBitmapButton* btnCbrt;
        wxBitmapButton* btnSqrt;
        wxBitmapButton* btnAC;
        wxBitmapButton* btn1;
        //*)

        DECLARE_EVENT_TABLE()

        void OnMenuItemClick(wxCommandEvent& event);
};

#endif // SCIENTIFIC_CALCULATORMAIN_H
